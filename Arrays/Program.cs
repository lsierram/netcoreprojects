﻿using System;
using System.Linq;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creates an array
            string[] cars = { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };

            Console.WriteLine("Original array:");
            foreach (string car in cars)
            {
                Console.WriteLine(car);

            }

            //Update
            cars[0] = "Mazda";
            Console.WriteLine("Modified array:");
            foreach (string car in cars)
            {
                Console.WriteLine(car);
            }

            //Sort (asc)
            Array.Sort(cars);
            Console.WriteLine("Sorted array:");
            foreach (string car in cars)
            {
                Console.WriteLine(car);
            }

            //Other common ways to create an array
            //Create an array of five elements, and add values later
            string[] emptyCarsArr = new string[5];

            // Create an array of five elements and add values right away 
            string[] luxuryCars = new string[5] { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };

            // Create an array of five elements without specifying the size 
            string[] fiveCars = new string[] { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };

            // Create an array of five elements, omitting the new keyword, and without specifying the size
            string[] newCars = { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };

            //Get the differences between two arrays
            string[] arrA = new string[] { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };
            string[] arrB = new string[] { "BMW", "Lexus", "Mercedez", "Toyota", "Porche" };

            //using System.Linq;
            var res = arrA.Union(arrB).Except(arrA.Intersect(arrB));

            Boolean isArrayEqual = arrA.SequenceEqual(arrB);

            if (!isArrayEqual)
            {
                Console.WriteLine("The difference between two arrays: ");
                foreach (string car in res)
                {
                    Console.WriteLine(car);
                }
            }
            else
            {
                Console.WriteLine("Arrays are equal");
            }

            //Concatenate two arrays
            string[] commonCars = { "Toyota", "Mazda", "Honda" };
            string[] costlyCars = { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };
            string[] combinedCars = commonCars.Concat(costlyCars).ToArray();

            Console.WriteLine("Combined cars are: ");
            foreach(string car in combinedCars)
            {
                Console.WriteLine(car);
            }

        }
    }
}
