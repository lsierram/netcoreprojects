﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace List
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The original list values are:");
            //Creating a list
            //using System.Collections.Generic;
            List<string> cars = new List<string>();

            //Add values to a List
            cars.Add("BMW");
            cars.Add("Lexus");
            cars.Add("Mercedez");
            cars.Add("Lamborghini");
            cars.Add("Porche");

            //Display List values
            foreach(string car in cars)
            {
                Console.WriteLine(car);
            }

            //Count the elements of a List
            int numberOfCars = cars.Count;
            Console.WriteLine("The numbers of cars are: " + numberOfCars);

            //Remove
            cars.Remove("BMW");
            Console.WriteLine("Removed BMW car from list:");
            foreach(string car in cars)
            {
                Console.WriteLine(car);
            }

            //Remove at
            cars.RemoveAt(cars.Count -1);
            Console.WriteLine("Removed last car from list:");
            foreach (string car in cars)
            {
                Console.WriteLine(car);
            }

            //Sort
            cars.Sort();
            Console.WriteLine("The sorted list values are:");
            foreach(string car in cars)
            {
                Console.WriteLine(car);
            }

            //Reverse
            cars.Reverse();
            Console.Write("Reversed List values are:");
            foreach (string car in cars)
            {
                Console.WriteLine(car);
            }

            //Search
            int index;
            index = cars.BinarySearch("Lexus");
            Console.WriteLine("The Lexus car was found at index: " + index);
            index = cars.BinarySearch("BMW");
            if (index == -1)
            {
                Console.WriteLine("The BMW car was no found");
            }

            //Join two list
            List<string> commonCars = new List<string> { "Toyota", "Mazda", "Honda", "Mercedez", };
            List<string> costlyCars = new List <string> { "BMW", "Lexus", "Mercedez", "Lamborghini", "Porche" };
            Console.WriteLine("The joined car lists is: ");
            //using System.Linq;
            var joinedCars = commonCars.Concat(costlyCars);
            foreach (string car in joinedCars)
            {
                Console.WriteLine(car);
            }

            //In Common Cars but no in Costly Cars
            var inCommonButNoOnCosltlyCars = commonCars.Except(costlyCars);
            Console.WriteLine("In Common Cars but no in costly cars");
            foreach (string car in inCommonButNoOnCosltlyCars)
            {
                Console.WriteLine(car);
            }

            //In Costly cars but no in Commons Cars
            var inCostlyCarsButNoOnCommunCars = costlyCars.Except(commonCars);
            Console.WriteLine("In Costly cars but no in Commons Cars");
            foreach (var car in inCostlyCarsButNoOnCommunCars)
            {
                Console.WriteLine(car);
            }

        }
    }
}
