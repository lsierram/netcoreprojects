﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args)
        {
            //Merge sort using recursion
            //Merge sort Time Complexity is O(nlogn), it comes from the while loop in the merge function
            //Merge sort is a divide and conquer algorithm that was invented by John von Neumann in 1945.
            
            //Define unsorted list and add values
            List<int> unsortedList = new List<int>();
            unsortedList.Add(1);
            unsortedList.Add(3);
            unsortedList.Add(2);
            unsortedList.Add(23);
            unsortedList.Add(17);
            unsortedList.Add(12);
            unsortedList.Add(49);
            unsortedList.Add(1);

            //Another way to add values to a list
            //List<int> unsortedList = new List<int> { 1, 3, 2, 23, 17, 12, 49, 1 };

            //Display unsorted values
            string unsorted = "";
            for (int i = 0; i < unsortedList.Count; i++)
            {
                unsorted += unsortedList[i] + " ";
            }

            //Other way to iterate through the values on a list
            //foreach (var item in unsortedList)
            //{
            //    unsorted += item + " ";
            //}

            Console.WriteLine($"The unsorted list is: {unsorted}");

            //Sort Values using recursion
            List<int> sortedList;
            sortedList = MergeSort(unsortedList);
            
            //Display sorted values
            string sorted = "";
            for (int i = 0; i < sortedList.Count(); i++)
            {
                sorted += sortedList[i] + " ";
            }
            Console.WriteLine($"The sorted list is: {sorted}");

        }

        /// <summary>
        /// Merge Sort using recursion
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <returns></returns>
         private static List<int> MergeSort(List<int> unsortedList)
        {
            int numberOfUnSortedValues = unsortedList.Count;
            if (numberOfUnSortedValues <= 1) 
                return unsortedList;

            //Create left and right lists
            List<int> left = new List<int>();
            List<int> right = new List<int>();

            //Get the middle index of the unsortedList
            int middle = numberOfUnSortedValues / 2;

            //Divide the unsortedList
            //Add values to the left and right list
            for (int i = 0; i < middle; i++)
            {
                left.Add(unsortedList[i]);
            }

            for (int j = middle; j < numberOfUnSortedValues; j++)
            {
                right.Add(unsortedList[j]);
            }

            left = MergeSort(left);
            right = MergeSort(right);

            return Merge(left, right);
        }

        /// <summary>
        /// Combines the list
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static List<int> Merge(List<int> left, List<int> right)
        {

            List<int> result = new List<int>();

            while(left.Count > 0 || right.Count > 0)
            {
                if(left.Count > 0 && right.Count > 0)
                {
                    //Compare element
                    if(left.First() <= right.First())
                    {
                        result.Add(left.First());
                        left.Remove(left.First());
                    }
                    else
                    {
                        result.Add(right.First());
                        right.Remove(right.First());
                    }

                }
                else if(left.Count > 0){ 
                    result.Add(left.First());
                    left.Remove(left.First());

                }else if(right.Count > 0)
                {
                    result.Add(right.First());
                    right.Remove(right.First());
                }
            }

            return result;

        }

    }
}
