﻿using System;
using System.Collections;

namespace Hash_Tables
{
    class Program
    {
        static Hashtable customerInfoHash;
        static void Main(string[] args)
        {
            //Creating a hash table
            customerInfoHash = new Hashtable();

            //Adding
            customerInfoHash.Add(1, "one");
            customerInfoHash.Add(7, "seven");
            customerInfoHash.Add(2, "two");

            //Removing
            if (customerInfoHash.ContainsKey(1))
            {
                customerInfoHash.Remove(1);
            }

            //Setting
            if (customerInfoHash.ContainsKey(2))
            {
                customerInfoHash[2] = "Two was replaced";
            }

            //Looping
            foreach(DictionaryEntry entry in customerInfoHash)
            {
                Console.WriteLine(" The key: " + entry.Key + " The value: " + entry.Value + "\n");
            }

        }
    }
}
