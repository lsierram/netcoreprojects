﻿using System;
using System.Collections.Generic;

namespace QuickSort
{
    class Program
    {
        static void Main(string[] args)
        {
            //Quick sort using recursion
            //Quick sort (pivot)
            //1. The pivot is a value that is pick from the array, usually the value in the middle (median-of-three)
            //2. Values from left are smallers
            //Quick sort Time Complexity are: Worth case: O(n^2), average case: O(nlogn)

            //Define unsorted list and add values
            List<int> unsortedList = new List<int>();
            unsortedList.Add(1);
            unsortedList.Add(7);
            unsortedList.Add(-8);
            unsortedList.Add(-11);
            unsortedList.Add(0);
            unsortedList.Add(16);
            unsortedList.Add(18);
            unsortedList.Add(65);
            unsortedList.Add(91);
            unsortedList.Add(3);

            //Another way to add values to a list
            //List<int> unsortedList = new List<int> { 1, 7, -8, -11, 0, 16, 18, 65, 91, 3 };


            //Display unsorted values
            string unsorted = "";
            for (int i = 0; i < unsortedList.Count; i++)
            {
                unsorted += unsortedList[i] + " ";
            }

            //Other way to iterate through the values on a list
            //foreach (var item in unsortedList)
            //{
            //    unsorted += item + " ";
            //}

            Console.WriteLine($"The unsorted list is: {unsorted}");

            //Sort Values using recursion
            QuickSort(unsortedList, 0, unsortedList.Count - 1);

            //Display sorted values
            string sorted = "";
            for (int i = 0; i < unsortedList.Count; i++)
            {
                sorted += unsortedList[i] + " ";
            }
            Console.WriteLine($"The sorted list is: {sorted}");
        }

        /// <summary>
        /// Quick Sort
        /// </summary>
        /// <param name="unsortedList"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        private static void QuickSort(List<int> unsortedList, int left, int right)
        {

            if (left < right)
            {
                int pivot = Partition(unsortedList, left, right);

                if (pivot > 1)
                {
                    QuickSort(unsortedList, left, pivot - 1);
                }
                if (pivot + 1 < right)
                {
                    QuickSort(unsortedList, pivot + 1, right);
                }
            }

        }

        /// <summary>
        /// Partition
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        private static int Partition(List<int> arr, int left, int right)
        {
            int pivot = arr[left];
            while (true)
            {

                while (arr[left] < pivot)
                {
                    left++;
                }

                while (arr[right] > pivot)
                {
                    right--;
                }

                if (left < right)
                {
                    if (arr[left] == arr[right]) return right;

                    int temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;

                }
                else
                {
                    return right;
                }
            }
        }
    }
}
