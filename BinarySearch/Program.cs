﻿using System;
using System.Collections.Generic;

namespace BinarySearch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Binary search
            //Divide and conquer olgarithm
            //Overage time complexity O(logn)

            List<int> listValues = new List<int>();
            listValues.Add(1);
            listValues.Add(2);
            listValues.Add(3);
            listValues.Add(4);
            listValues.Add(5);
            listValues.Add(6);
            listValues.Add(7);
            listValues.Add(8);
            listValues.Add(9);

            var result = BinarySearchRecursive(listValues, 2, 0, listValues.Count - 1);
            //var result = BinarySearchIterative(listValues, 2);
            Console.WriteLine("Found Values: " + result);
        }

        /// <summary>
        /// Using recursion
        /// </summary>
        /// <param name="inputArray"></param>
        /// <param name="key"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static object BinarySearchRecursive(List<int> inputArray, int key, int min, int max)
        {
            if(min > max)
            {
                return "Nil";
            }
            else
            {
                int mid = (min + max) / 2;
                if (key == inputArray[mid])
                {
                    return ++mid;
                }else if(key < inputArray[mid])
                {
                    return BinarySearchRecursive(inputArray, key, min, mid - 1);
                }else
                {
                    return BinarySearchRecursive(inputArray, key, mid + 1, max);
                }
            }
        }

        /// <summary>
        /// Using iteration
        /// </summary>
        /// <param name="inputList"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object BinarySearchIterative(List<int> inputList, int key)
        {
            int min = 0;
            int max = inputList.Count - 1;
            while (min <= max)
            {
                int mid = (min + max) / 2;
                if (key == inputList[mid])
                {
                    return ++mid;
                }
                else if (key < inputList[mid])
                {
                    max = mid - 1;
                }
                else
                {
                    min = mid + 1;
                }
            }
            return null;
        }

    }
}
