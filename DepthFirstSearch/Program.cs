﻿using System;
using System.Collections.Generic;

class Graph
{
	//# vertices
	private int V; 

	private List<int>[] adj;

	/// <summary>
	/// Constructor definition
	/// </summary>
	/// <param name="v"></param>
	Graph(int v)
	{
		V = v;
		adj = new List<int>[v];
		for (int i = 0; i < v; ++i)
			adj[i] = new List<int>();
	}


	/// <summary>
	/// Adding edge into the graph
	/// </summary>
	/// <param name="v"></param>
	/// <param name="w"></param>
	void AddEdge(int v, int w)
	{
		// Add w in v
		adj[v].Add(w); 
	}

	/// <summary>
	///  Recursive DFS
	/// </summary>
	/// <param name="v"></param>
	/// <param name="isVisited"></param>
	void DepthFirstSearchRec(int v, bool[] isVisited)
	{
		// Mark current node as visited

		isVisited[v] = true;
		Console.WriteLine(v + " ");


		List<int> vList = adj[v];
		foreach (var val in vList)
		{
			if (!isVisited[val])
				DepthFirstSearchRec(val, isVisited);
		}
	}

	/// <summary>
	/// The function to do DFS traversal.
	// It uses recursive DepthFirstSearchRec
	/// </summary>
	/// <param name="t"></param>
	void DepthFirstSearch(int t)
	{

		bool[] visited = new bool[V];

		DepthFirstSearchRec(t, visited);
	}

	public static void Main(String[] args)
	{
		Graph g = new Graph(4);

		//Define edges
		g.AddEdge(0, 2);
		g.AddEdge(0, 4);
		g.AddEdge(1, 2);
		g.AddEdge(2, 0);
		g.AddEdge(2, 7);
		g.AddEdge(3, 2);

		Console.WriteLine("DFS is ");

		g.DepthFirstSearch(2);

		Console.ReadKey();
	}
}

