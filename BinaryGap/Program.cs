﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BinaryGap
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Binary gap within a positive integer N is any maximal sequence of consecutive
            //zeros that is surrounded by ones at both ends in the binary representation of N
            Console.WriteLine("Binary Gap: \n");
            Console.WriteLine("1041 :" + Solution(1041) + "\n"); // Return 5 10000010001
            Console.WriteLine("32 :" + Solution(32) + "\n"); // Return 0 100000
            Console.WriteLine("20 :" + Solution(20) + "\n"); // Return 1 10100
        }

        public static int Solution(int N) {
            List<int> gaps = new List<int>();

            if ((N == (int)N) && (N >= 1 & N <= 2147483647))
            {
                var binary = Convert.ToString(N,2);
                int length = binary.Length;


                Boolean hasOne = false;
                int zeros = 0;
                for (int i = 0; i < length; i++)
                {
                   if(binary[i] == '1')
                    {
                        if (!hasOne)
                        {
                            hasOne = true;
                            continue;
                        }

                        gaps.Add(zeros);
                        zeros = 0;
                        continue;

                    }

                   zeros++;
                }
            }
            return (gaps.Count == 0) ? 0: gaps.Max(); 
        }
    }
}
