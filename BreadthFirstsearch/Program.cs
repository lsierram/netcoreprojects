﻿using Microsoft.Azure.Management.ResourceManager.Fluent.Core.DAG;
using System;
using System;
using System.Collections.Generic;

namespace BreadthFirstsearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    namespace BFS
    {
        class Graph
        {
            private int _NumberOfEdges;
            //private bool _directed;
            LinkedList<int>[] _adj;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="V"></param>
            public Graph(int V)
            {
                _adj = new LinkedList<int>[V];

                for (int i = 0; i < _adj.Length; i++)
                {
                    _adj[i] = new LinkedList<int>();
                }

                _NumberOfEdges = V;
                //_directed = directed;
            }

            /// <summary>
            /// Add Edge
            /// </summary>
            /// <param name="v"></param>
            /// <param name="w"></param>
            public void AddEdge(int v, int w)
            {
                _adj[v].AddLast(w);

                //if (!_directed)
                //{
                //    _adj[w].AddLast(v);
                //}
            }

            /// <summary>
            /// Implement BFS
            /// </summary>
            /// <param name="s"></param>
            public void BreadthFirstSearch(int s)
            {
                bool[] visited = new bool[_NumberOfEdges];
                for (int i = 0; i < _NumberOfEdges; i++)
                    visited[i] = false;

                // Create a queue for BFS
                LinkedList<int> queue = new LinkedList<int>();

                visited[s] = true;
                queue.AddLast(s);

                while (queue.Any())
                {
                    // Dequeue the vertex from queue and dislay it
                    s = queue.First();
                    Console.Write(s + " ");
                    queue.RemoveFirst();

                    LinkedList<int> list = _adj[s];

                    foreach (var val in list)
                    {
                        if (!visited[val])
                        {
                            visited[val] = true;
                            queue.AddLast(val);
                        }
                    }
                }
            }

        }

        class Program
        {
            static void Main(string[] args)
            {
                Graph g = new Graph(7);
                g.AddEdge(0, 1);
                g.AddEdge(0, 2);
                g.AddEdge(0, 3);
                g.AddEdge(1, 0);
                g.AddEdge(1, 5);
                g.AddEdge(2, 5);
                g.AddEdge(3, 0);
                g.AddEdge(3, 4);
                g.AddEdge(4, 6);
                g.AddEdge(5, 1);
                g.AddEdge(6, 5);

                Console.Write("BFT from vertex 5:\n");
                g.BreadthFirstSearch(5);
            }
        }
    }

}

