﻿using System;

namespace Factorial
{
    class Program
    {
        static void Main(string[] args)
        {

            while (true)
            {
                //This console app calculates a factorial giving an user input.
                //0! = 1
                //1! = 1
                //2! = 2 * 1! = 2
                //3! = 3 * 2! = 6... 
                //n! = n * (n - 1)!

                //Type a number press enter
                Console.WriteLine("Enter a whole number (e.g. 0, 1, 2, 3 ...) to calculate its factorial:");
                // Create a string variable and get user input from the keyboard and store it in the variable
                int inValue = Convert.ToInt32(Console.ReadLine());
                long fibNum = CalculateFactorial(inValue);
                Console.WriteLine("The factorial number is: " + fibNum);
            }

        }
        /// <summary>
        /// Calculates factorial using recursion
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        static long CalculateFactorial(long n)
        {
            if (n == 0)
                return 1;

            return n * CalculateFactorial(n - 1);
        }

        /// <summary>
        /// Calculates factorial (non recursive)
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        //public long CalculateFactorial(int n)
        //{
        //    if (n == 0)
        //        return 1;
        //    long value = 1;
        //    for (int i = n; i > 0; i--)
        //    {
        //        value *= i;
        //    }
        //    return value;
        //}
    }
}
